const config = {
    host: "localhost",
    port: 28015,
    db: "fnclash",
    max: 500,
    buffer: 5,
    timeoutGb: 60 * 1000
}

const r = require('rethinkdbdash')(config);
module.exports = { db: r }