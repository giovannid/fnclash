const redis = require('redis'),
      rc = redis.createClient();
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

rc.on("error", function (err) {
    console.log("Error " + err);
});

module.exports = rc;