const express = require('express');
const router = express.Router();

const moment = require('moment');

const { passport, authenticator } = require('./passport.config');

const Tournaments = require('./tournaments.model');
const Users = require('./users.model');

router.use(passport.initialize());

router.get('/list/:started?', authenticator({ roles: ['player', 'admin', 'superadmin'] }), (req, res) => {
    
    Tournaments.getTournaments(req.params.started)
    .then(results => {
        return res.json({ code: 200, result: { tourinfo: results }, message: '' });
    }).catch(err => {
        return res.json({ code: 500, result: { error: true }, message: 'Unable to get list of tournaments.' });
    });
    
});

router.get('/info/:tourid', authenticator({ roles: ['player', 'admin', 'superadmin'] }), (req, res) => {

    Tournaments.getTournament(req.params.tourid)
    .then(result => {
        return res.json({ code: 200, result: { tourinfo: result }, message: '' });
    }).catch(err => {
        return res.json({ code: 500, result: { error: true }, message: 'Unable to get tournament information.' });
    });

});

router.get('/info/:tourid/txn', authenticator({ roles: [ 'admin', 'superadmin'] }), (req, res) => {

    Tournaments.getTournamentTxns(req.params.tourid)
    .then(result => {
        return res.json({ code: 200, result: { tourinfo: result }, message: '' });
    });

});

router.post('/registeruser', authenticator({ roles: [ 'player', 'admin', 'superadmin'] }), (req, res) => {
    if(!req.body.tourid) {
        return res.json({ code: 401, result: { error: true }, message: 'Required Fields Missing.' });
    }

    let tour_id = req.body.tourid;
    let user_id = req.payload.id;

    Tournaments.isUserRegistered(tour_id, user_id)
    .then(result => {
        if(result > 0) return res.json({ code: 200, result: { already_registered: true }, message: 'User already registered in tournament.' });

        // not registered
        else {
            // check if they have enough credits for this tournament
            Promise.all([
                Users.getUserCredits(user_id), 
                Tournaments.getTournamentFee(tour_id)
            ])
            .then(([user_creds, tour_fee]) => {
                if(tour_fee === null) {
                    return res.json({ code: 200, result: { tournament_error: true }, message: 'This tournament doesn\'t exist' });
                }

                if(user_creds >= tour_fee) {
                    // register user in the tournament
                    Tournaments.registerUser(tour_id, user_id, tour_fee)
                    .then(registered => {
                        // if max player is reached
                        if(registered.max_registered && registered.max_registered == true) {
                            return res.json({ code: 200, result: { max_registered: true }, message: 'Max num player reached' });
                        }
                        // if the user is registered
                        if(registered.inserted && registered.inserted === 1) {
                            // sub credits from their account
                            Users.subCredits(user_id, tour_fee)
                            .then(sub_credit => {
                                // if this is successfull, return successful msg
                                if(sub_credit.replaced && sub_credit.replaced === 1) {
                                    return res.json({ code: 200, result: { user_registered: true }, message: '' });
                                } else {
                                    // if we can't for some reason subtract credits, undo registration and return error msg
                                    Tournaments.undoRegistration(tour_id, user_id)
                                    .then(undo_registration => {
                                        return res.json({ code: 500, result: { unable_to_register: true }, message: 'Unable to register user in the tournament.' });
                                    })
                                }
                            })
                        }
                    });
                } else {
                    return res.json({ code: 200, result: { no_credits: true }, message: 'You don\'t have enough credits to register in this tournament' });
                }
            });
        }
    }).catch(err => {
        console.log(err);
        return res.json({ code: 500, result: { error: true }, message: 'Unable to register user in the tournament.' });
    });
});

router.post('/register', authenticator({ roles: [ 'admin', 'superadmin'] }), (req, res) => {
    if(!req.body.fee || !req.body.queuetype || !req.body.maxplayers || !req.body.name || !req.body.start_date) {
        return res.json({ code: 401, result: { error: true }, message: 'Required Fields Missing.' });
    }

    Tournaments.registerTournament({
        'fee': req.body.fee,
        'queuetype': req.body.queuetype,
        'maxplayers': req.body.maxplayers, 
        'creation': moment().unix(),
        'started': false,
        'name': req.body.name,
        'winner': "",
        'start_date': req.body.start_date
    })
    .then(register_tour => {
        if(register_tour && register_tour.inserted === 1) {
            return res.json({ code: 200, result: { tournament_created: true }, message: '' });
        } else {
            return res.json({ code: 500, result: { error: true }, message: 'Unable to create new tournament.' });
        }
    }).catch(err => {
        console.log(err);
        return res.json({ code: 500, result: { error: true }, message: 'Unable to create new tournament.' });
    });
});

router.post('/update', authenticator({ roles: [ 'admin', 'superadmin'] }), (req, res) => {
    if(!req.body.tourid || !req.body.fee || !req.body.queuetype || !req.body.maxplayers || !req.body.name || !req.body.start_date) {
        return res.json({ code: 401, result: { error: true }, message: 'Required Fields Missing.' });
    }

    res.json('disabled');
    
});

module.exports = router;