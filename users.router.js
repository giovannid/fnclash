const express = require('express');
const router = express.Router();

const crypto = require('crypto');
const moment = require('moment');
const jwt = require('jsonwebtoken');

const { passport, authenticator, jwtkey, pass_config } = require('./passport.config');

const Users = require('./users.model');

router.use(passport.initialize());

router.get('/info/:username', authenticator({ roles: ['player', 'admin', 'superadmin'] }), (req, res) => {
    if(req.payload.role == 'player' && req.payload.username != req.params.username) {
        return res.json({ code: 500, result: { error: true }, message: 'You don\'t have enough permissions for this action.' });
    }
    
    Users.getUser({
        'username': req.params.username
    }, ['password'])
    .then(results => {
        return res.json({ code: 200, result: results[0] || {}, message: '' });
    }).catch(err => {
        if(err) return res.json({ code: 500, result: { error: true }, message: 'Unable to get user information.' });
    });
   
});

router.post('/auth', (req, res) => {
    if(!req.body.username || !req.body.password){
        return res.json({ code: 401, result: { error: true }, message: 'Missing Username and/or Password' });
    }
    
    Users.getUser({
        'username': req.body.username
    })
    .then(results => {
        if(results[0]) {
            let userinfo = results[0];
            
            crypto.pbkdf2(req.body.password, userinfo.password.salt, pass_config.pass_iterations, pass_config.pass_length, pass_config.pass_digest, (err, hash) => {
                if(err) throw err;
                
                if(hash.toString(pass_config.pass_encoding) === userinfo.password.hash) {
                    let payload = {
                        id: results[0].id,
                        role: results[0].role,
                    }
                    let token = jwt.sign(payload, jwtkey, {algorithm: pass_config.token_algorithm});
                    token = token.slice(token.indexOf('.')+1);
                    payload.credits = results[0].credits;
                    payload.creation = results[0].creation;
                    payload.email = results[0].email;
                    payload.teams = results[0].teams;
                    payload.username = results[0].username;
                    
                    return res.json({ code: 200, result: { token: token, userinfo: payload }, message: '' });
                } else {
                    return res.json({ code: 401, result: { error: true }, message: 'Incorrect Username and/or Password.' });
                }
            });
        } else {
            return res.json({ code: 401, result: { error: true }, message: 'Incorrect Username and/or Password.' });
        }
    }).catch(err => {
        console.log(err);
        if(err) return res.json({ code: 500, result: { error: true }, message: 'Unable to get user information.' });
    });
   
});

router.post('/register', (req, res) => {
    if(!req.body.username || !req.body.password || !req.body.email){
        return res.json({ code: 401, result: { error: true }, message: 'Required Fields Missing.' });
    }

    Users.checkExistingUser(req.body.username, req.body.email)
    .then(check_for_username => {
        if(check_for_username > 0) {
            return res.json({ code: 200, result: { account_exists: true }, message: 'Username/Email already being used by an account.' });
        } else {
            let salt = crypto.randomBytes(pass_config.pass_salt_length).toString(pass_config.pass_encoding);
            crypto.pbkdf2(req.body.password, salt, pass_config.pass_iterations, pass_config.pass_length, pass_config.pass_digest, (err, hash) => {
                if(err) return res.json({ code: 500, result: { error: true }, message: 'Unable to register user.' });
                
                Users.registerUser({
                    username: req.body.username,
                    email: req.body.email,
                    password: { hash: hash.toString(pass_config.pass_encoding) , salt: salt },
                    role: 'player',
                    creation: moment().unix(),
                    credits: 0,
                    teams: []
                })
                .then(result => {
                    if(result.inserted === 1) {
                        return res.json({ code: 200, result: { user_registered: true }, message: '' });
                    } else {
                        return res.json({ code: 500, result: { error: true }, message: 'Unable to register user.' });
                    }
                });
                
            });
        }
    }).catch(err => {
        return res.json({ code: 500, result: { error: true }, message: 'Unable to register user' });
    });
});

router.post('/update', authenticator({ roles: ['player', 'admin', 'superadmin'] }), (req, res) => {
    if(!req.body.email && !req.body.password){
        return res.json({ code: 401, result: { error: true }, message: 'Required Fields Missing.' });
    }

    if(!req.body.password && req.body.email) {
        Users.updateUser(
            req.payload.id, 
        {
            email: req.body.email,
        })
        .then(result => {
            if(result.replaced === 1 || result.unchanged === 1) {
                return res.json({ code: 200, result: { user_updated: true }, message: '' });
            } else {
                return res.json({ code: 500, result: { error: true }, message: 'Unable to update user.' });
            }
        }).catch(err => {
            return res.json({ code: 500, result: { error: true }, message: 'Unable to update user.' });
        });
    } else {
        let salt = crypto.randomBytes(pass_config.pass_salt_length).toString(pass_config.pass_encoding);
        crypto.pbkdf2(req.body.password, salt, pass_config.pass_iterations, pass_config.pass_length, pass_config.pass_digest, (err, hash) => {
            if(err) return res.json({ code: 500, result: { error: true }, message: 'Unable to update user.' });

            Users.updateUser(
                req.payload.id, 
            {
                email: req.body.email,
                password: { hash: hash.toString(pass_config.pass_encoding) , salt: salt }
            })
            .then(result => {
                if(result.replaced === 1) {
                    return res.json({ code: 200, result: { user_updated: true }, message: '' });
                } else {
                    return res.json({ code: 500, result: { error: true }, message: 'Unable to update user.' });
                }
            }).catch(err => {
                return res.json({ code: 500, result: { error: true }, message: 'Unable to update user.' });
            });
                    
        });
    }
});

router.get('/me', authenticator({ roles: ['player', 'admin', 'superadmin'] }), (req, res) => {
    Users.getUser({
        'id': req.payload.id
    }, ['password', 'resetkey'])
    .then(results => {
        return res.json({ code: 200, result: results || {}, message: '' });
    }).catch(err => {
        if(err) return res.json({ code: 500, result: { error: true }, message: 'Unable to get user information.' });
    });
});

router.post('/forgot', (req, res) => {
    if(!req.body.email){
        return res.json({ code: 401, result: { error: true }, message: 'Required Field Missing.' });
    }

    Users.getUser({
        'email': req.body.email
    })
    .then(result => {
        if(result[0]) {
            Users.createResetKey(result[0].id)
            .then(tokeninfo => {
                if(tokeninfo.db.inserted === 1) {
                    // ADD SEND MAIL JOB TO QUEUE HERE -> tokeninfo.resetkey
                    console.log(tokeninfo.resetkey);
                    return res.json({ code: 200, result: { resetkey_created: true }, message: '' });
                } else {
                    return res.json({ code: 500, result: { error: true }, message: 'Unable to create reset link.' });
                }
            });
        } else {
            return res.json({ code: 401, result: { error: true }, message: 'Email not found.' });
        }
    }).catch(err => {
        console.log(err);
        return res.json({ code: 500, result: { error: true }, message: 'Unable to find user.' });
    });
});

router.get('/reset/:resetkey', (req, res) => {
    if(!req.params.resetkey) {
        return res.json({ code: 500, result: { error: true }, message: 'Missing Token Information.' });
    }

    Users.checkResetKey(req.params.resetkey)
    .then(result => {
        if(result[0] && result[0].used === false && !moment.unix(result[0].expiration).isBefore()) {
            return res.json({ code: 200, result: { valid_token: true } || {}, message: '' });
        } else {
            return res.json({ code: 401, result: { error: true }, message: 'Invalid Token.' });
        }
    }).catch(err => {
        if(err) return res.json({ code: 500, result: { error: true }, message: 'Unable to get user information.' });
    });
});

router.post('/reset/:resetkey', (req, res) => {
    if(!req.params.resetkey && !req.body.newpassword && !req.body.confirmpassword){
        return res.json({ code: 401, result: { error: true }, message: 'Required fields missing.' });
    }

    Users.checkResetKey(req.params.resetkey)
    .then(result => {
        if(result[0] && result[0].used === false && !moment.unix(result[0].expiration).isBefore()) {
            
            if(req.body.newpassword === req.body.confirmpassword) {
                Users.setUsedResetKey(req.params.resetkey);
                let salt = crypto.randomBytes(pass_config.pass_salt_length).toString(pass_config.pass_encoding);
                crypto.pbkdf2(req.body.newpassword, salt, pass_config.pass_iterations, pass_config.pass_length, pass_config.pass_digest, (err, hash) => {
                    if(err) return res.json({ code: 500, result: { error: true }, message: 'Unable to update user.' });

                    Users.updateUser(
                        result[0].user_id, 
                    {
                        password: { hash: hash.toString(pass_config.pass_encoding) , salt: salt }
                    })
                    .then(result => {
                        if(result.replaced === 1) {
                            return res.json({ code: 200, result: { password_changed: true } || {}, message: '' });
                        } else {
                            return res.json({ code: 500, result: { error: true }, message: 'Unable to update user.' });
                        }
                    }).catch(err => {
                        return res.json({ code: 500, result: { error: true }, message: 'Unable to update user.' });
                    });
                            
                });
            } else {
                return res.json({ code: 401, result: { error: true }, message: 'Passwords doesn\'t match.' });
            }

        } else {
            return res.json({ code: 401, result: { error: true }, message: 'Invalid Token.' });
        }
    }).catch(err => {
        if(err) return res.json({ code: 500, result: { error: true }, message: 'Unable to get user information.' });
    });
});

module.exports = router;