//const rc = require('./redis.config');
const moment = require('moment');
const sid = require('shortid');
const async = require('async');

const Users = require('./users.model');

class Transactions{
    
    static newClashCoinTransaction(username, amount) {
        return new Promise((resolve, reject) => {
            let txnid = sid.generate();
            rc.hmsetAsync(`txn:${txnid}`, 
            'user', username,
            'creation', moment().unix(),
            'id', txnid,
            'amount', amount)
            .then(txncreated => {
                if(txncreated !== 'OK') resolve(false);
                resolve(txnid);
            })
            .catch(err => reject(err));
        });
    }
    
}

module.exports = Transactions;