const passport = require("passport");
const passportJWT = require("passport-jwt");

const { db } = require('./rethink.config');

const crypto = require('crypto');

const JwtStrategy = passportJWT.Strategy;

const pass_config = {
    pass_length: 512,
    pass_salt_length: 128,
    pass_iterations: 10000,
    pass_digest: 'sha512',
    pass_encoding: 'base64',
    token_algorithm: 'HS512',
    token_header: { 'alg': 'HS512', 'typ': 'JWT' },
    resetkey_length: 32,
    resetkey_encoding: 'hex'
}

const jwtOptions = {}
jwtOptions.jwtFromRequest = (request) => {
    const auth_scheme_lower = 'bearer';
    const re = /(\S+)\s+(\S+)/;
    let token = null;

    function parseAuthHeader(hdrValue) {
        if (typeof hdrValue !== 'string') {
            return null;
        }
        var matches = hdrValue.match(re);
        return matches && { scheme: matches[1], value: matches[2] };
    }

    if (request.headers['authorization']) {
        let auth_params = parseAuthHeader(request.headers['authorization']);
        if (auth_params && auth_scheme_lower === auth_params.scheme.toLowerCase()) {
            token = `${Buffer.from(JSON.stringify(pass_config.token_header)).toString('base64')}.${auth_params.value}`;
        }
    }
    return token;
}
jwtOptions.secretOrKey = 'tasmanianDevil';
//jwtOptions.jsonWebTokenOptions = { maxAge: '1m' };
const jwtkey = jwtOptions.secretOrKey;

passport.use(new JwtStrategy(jwtOptions, function(jwt_payload, next) {
    console.log('');
    console.log(`payload received ${JSON.stringify(jwt_payload)}`);

    db.table('users')
    .get(jwt_payload.id)
    .pluck('role', 'username', 'credits', 'creation', 'id')
    .default(null)
    .run()
    .then(result => {
        if(!result) {
            next(null, false);
            return false;
        } else {
            next(null, result);
            return result;
        }
    }).catch(err => {
        next(err);
        return null;
    });

}));

function authenticator(options) {
    return function(req, res, next) {
        passport.authenticate('jwt', { session: false }, (err, user, info) => {

            console.log(`endpoint: ${req.baseUrl + req.url} | permissions: ${options.roles}`);
            console.log(`data received: ${JSON.stringify(req.body)}`);
            
            if(err) return res.json({ code: 500, result: { error: true }, message: 'Unable to authenticate user.' });
            if(!user || !options.roles.includes(user.role)) return res.json({ code: 401, result: { error: true }, message: 'Unauthorized Access.' } );
            
            req.payload = { username: user.username, id: user.id, role: user.role, creation: user.creation };

            return next();
        })(req, res, next);
    }
}

module.exports = { passport, authenticator, jwtkey, pass_config };