const moment = require('moment');
const { db } = require('./rethink.config');

const Users = require('./users.model');
const Transactions = require('./transactions.model');

class Tournaments{
    
    static getTournament(id) {
        return db
        .table('tournaments')
        .get(id)
        .default([])
        .merge((tour) => {
            return {
                registered: db.table('tournaments_txn').getAll(tour('id'), {index: 'tournament_id'}).count()
            }
        })
        .run()
    }

    static getTournaments(started = false) {
        const filterStarted = (started) ? true : false;
        return db
        .table('tournaments')
        .filter({started: started})
        .run()
    }

    static getTournamentTxns(id) {
        return db
        .table('tournaments')
        .get(id)
        .default([])
        .merge(tour => {
            return {
                txns: db.table('tournaments_txn').getAll(tour('id'), {index: 'tournament_id'})
                    .merge(txn => {
                        return {
                            user: db.table('users').get(txn('user_id')).without('password')
                        }
                    })
                    .without('user_id')
                    .coerceTo('array')
            }
        })
        .run()
    }

    static isUserRegistered(id, user_id) {
        return db
        .table('tournaments_txn')
        .filter({ 
            tournament_id: id, 
            user_id: user_id
        })
        .count()
        .run()
    }

    static getTournamentFee(id) {
        return db
        .table('tournaments')
        .get(id)('fee')
        .default(null)
        .run()
    }

    static registerUser(id, user_id, fee) {

        // check num of registered players and if it's possible to register
        return this.getTournament(id)
        .then(tour_info => {
            if(tour_info.registered < tour_info.maxplayers) {
                // register player
                return db.table('tournaments_txn')
                .insert({
                    creation: moment().unix(),
                    amount: fee,
                    tournament_id: id,
                    user_id: user_id
                })
                .run()
            } else {
                return Promise.resolve({ max_registered: true });
            }
        });
        
    }

    static undoRegistration(id, user_id) {
        return db
        .table('tournaments_txn')
        .filter({
            tournament_id: id,
            user_id: user_id
        })
        .delete()
        .run()
    }

    static registerTournament(fields) {
        return db
        .table('tournaments')
        .insert({...fields})
        .run()
    }

    static updateTournament(fields) {
        return null;
    }

}

module.exports = Tournaments;