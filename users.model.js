const moment = require('moment');
const { db } = require('./rethink.config');
const { pass_config } = require('./passport.config');
const crypto = require('crypto');

class Users{

    static async getUser(input, without = []) {
        if(input.id) return await this.getUserById(input.id, without);
        if(input.username) return await this.getUserByUsername(input.username, without);
        if(input.email) return await this.getUserByEmail(input.email, without);
        return null;
    }

    static getUserByUsername(username, without) {
        return db
        .table('users')
        .getAll(username, { index: 'username' })
        .merge(row => {
            return {
                teams: row('teams').map(teamid => {
                    return db.table('teams').get(teamid);
                })
            }
        })
        .without(without)
        .run();
    }

    static getUserById(id, without) {
        return db
        .table('users')
        .get(id)
        .merge(row => {
            return {
                teams: row('teams').map(teamid => {
                    return db.table('teams').get(teamid);
                })
            }
        })
        .without(without)
        .run();
    }

    static getUserByEmail(email, without) {
        return db
        .table('users')
        .getAll(email, { index: 'email' })
        .merge(row => {
            return {
                teams: row('teams').map(teamid => {
                    return db.table('teams').get(teamid);
                })
            }
        })
        .without(without)
        .run();
    }

    static checkExistingUser(username, email) {
        return db
        .table('users')
        .filter(db.row('username').eq(username).or(db.row('email').eq(email)))
        .count()
        .run();
    }

    static registerUser(fields) {
        return db
        .table('users')
        .insert({...fields})
        .run();
    }

    static updateUser(id, fields) {
        return db
        .table('users')
        .get(id)
        .update({...fields})
        .run(); 
    }
    
    static getUserCredits(id) {
        return db
        .table('users')
        .get(id)('credits')
        .run();
    }

    static addCredits(id, amount) {
        return db
        .table('users')
        .get(id)
        .update({
            credits: db.row('credits').add(amount)
        })
        .run();
    }

    static subCredits(id, amount) {
        return this.getUserCredits(id)
        .then(current_credit => {
            if((current_credit - amount) <= 0) {
                return db
                .table('users')
                .get(id)
                .update({
                    credits: 0
                })
                .run();
            } else {
                return db
                .table('users')
                .get(id)
                .update({
                    credits: db.row('credits').sub(amount)
                })
                .run();
            }
        });
    }

    static async createResetKey(id) {
        let resetkey = crypto.randomBytes(pass_config.resetkey_length).toString(pass_config.resetkey_encoding);
        let hash_resetkey = crypto.createHash('sha512').update(resetkey).digest('hex');
        let rdb = await db
        .table('users_resetkeys')
        .insert({
            resetkey: hash_resetkey,
            user_id: id,
            creation: moment().unix(),
            expiration: moment().add(20, 'm').unix(),
            used: false
        })
        .run();
        return Promise.resolve({ resetkey: resetkey, db: rdb });
    }

    static checkResetKey(resetkey) {
        let hash_resetkey = crypto.createHash('sha512').update(resetkey).digest('hex');
        return db
        .table('users_resetkeys')
        .getAll(hash_resetkey, { index: 'resetkey' })
        .run();
    }

    static setUsedResetKey(resetkey) {
        let hash_resetkey = crypto.createHash('sha512').update(resetkey).digest('hex');
        return db
        .table('users_resetkeys')
        .getAll(hash_resetkey, { index: 'resetkey' })
        .update({
            used: true
        })
        .run();
    }
}

module.exports = Users;