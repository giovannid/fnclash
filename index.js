// EXPRESS
const express = require("express");
const bodyParser = require("body-parser");

//ROUTERS
const users_router = require('./users.router');
const tours_router = require('./tournaments.router');

const cors = require('cors');

// INITIALIZE EXPRESS
const app = express();

// EXPRESS OPTIONS
app.use(cors())
app.use(bodyParser.json({
    type: 'application/json'
}));
app.use((err, req, res, next) => {
    if(err.status == 400) 
        return res.json({ code: 500, result: { error: true }, message: 'Invalid Data' });
    
    return next(err);
});

// USERS
app.use('/users', users_router);
app.use('/tournaments', tours_router);

app.get("/", function(req, res) {
    res.json({message: "Express is up!"});
});

// START APP
app.listen(3000, function() {
    console.log("Express running on port 3000");
});